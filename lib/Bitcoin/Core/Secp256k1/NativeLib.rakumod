unit module Bitcoin::Core::Secp256k1::NativeLib;

use LibraryCheck;
use Bitcoin::Core::Secp256k1::Exception;

constant libraryname = 'secp256k1';

sub secp256k1-lib is export {
    my @libsvers = (v0, v1, v2, v5);

    state $lib;

    for @libsvers.values -> $version {
        if library-exists(libraryname, $version) {
            $lib = sprintf("lib%s.so.%d", libraryname, $version.parts.head);

            last;
        }
    }

    if !$lib {
        die X::Library.new(:payload(sprintf("Cannot find %s shared library", libraryname)));
    }

    return $lib;
}
