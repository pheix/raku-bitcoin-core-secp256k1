module Bitcoin::Core::Secp256k1::Exception {
    class X is Exception {
        has Str $.payload is default('default secp256k1 exception');

        method message returns Str {
            return sprintf("%s: %s", self.^name, $!payload);
        }
    }

    class X::Library    is X {};
    class X::PrivateKey is X {};
    class X::PublicKey  is X {};
    class X::Message    is X {};
    class X::Message::Signature is X::Message {};
    class X::Message::Signature::Serialize is X::Message::Signature {};
    class X::Message::Signature::Convert   is X::Message::Signature {};
}
