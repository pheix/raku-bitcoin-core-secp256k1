unit class Bitcoin::Core::Secp256k1;

use NativeCall;
use Bitcoin::Core::Secp256k1::NativeLib;
use Bitcoin::Core::Secp256k1::Exception;

has Str   $.lib is default(secp256k1-lib);
has uint8 @.key[64];
has uint8 @.sig[64];
has uint8 @.sig_n[64];
has uint8 @.sig_r[65];

class secp256k1_pubkey is repr('CStruct') {
    HAS uint8 @.data[64] is CArray;
}

class secp256k1_ecdsa_signature is repr('CStruct') {
    HAS uint8 @.data[64] is CArray;
}

class secp256k1_ecdsa_recoverable_signature is repr('CStruct') {
    HAS uint8 @.data[65] is CArray;
}

method convert_carray(CArray :$arr, UInt :$len) returns List {
    my @ret;

    for ^$len {
        my $member = $arr[$_];
        @ret.push(($member < 0) ?? (256 + $member) !! $member);
    }

    return @ret;
}

method ctx(Str :$type) returns Pointer {
    my $SECP256K1_FLAGS_TYPE_CONTEXT           = 1 +< 0;
    my $SECP256K1_FLAGS_BIT_CONTEXT_VERIFY     = 1 +< 8;
    my $SECP256K1_FLAGS_BIT_CONTEXT_SIGN       = 1 +< 9;
    my $SECP256K1_FLAGS_BIT_CONTEXT_DECLASSIFY = 1 +< 10;

    my $SECP256K1_CONTEXT_VERIFY     = $SECP256K1_FLAGS_TYPE_CONTEXT +| $SECP256K1_FLAGS_BIT_CONTEXT_VERIFY;
    my $SECP256K1_CONTEXT_SIGN       = $SECP256K1_FLAGS_TYPE_CONTEXT +| $SECP256K1_FLAGS_BIT_CONTEXT_SIGN;
    my $SECP256K1_CONTEXT_DECLASSIFY = $SECP256K1_FLAGS_TYPE_CONTEXT +| $SECP256K1_FLAGS_BIT_CONTEXT_DECLASSIFY;
    my $SECP256K1_CONTEXT_NONE       = $SECP256K1_FLAGS_TYPE_CONTEXT;

    my $SECP256K1_CONTEXT = $SECP256K1_CONTEXT_SIGN;

    if $type eq 'sign' {
        $SECP256K1_CONTEXT = $SECP256K1_CONTEXT_SIGN;
    }
    elsif $type eq 'verify' {
        $SECP256K1_CONTEXT = $SECP256K1_CONTEXT_VERIFY;
    }
    elsif $type eq 'declassify' {
        $SECP256K1_CONTEXT = $SECP256K1_CONTEXT_DECLASSIFY;
    }
    else {
        $SECP256K1_CONTEXT = $SECP256K1_CONTEXT_NONE;
    }

    my $ptr = secp256k1_context_create($SECP256K1_CONTEXT);

    return $ptr;
}

method create_public_key(Str :$privkey!) returns buf8 {
    die X::PrivateKey.new(:payload('invalid hex')) unless $privkey && $privkey ~~ m/^^ <xdigit> ** 64 $$/;

    my $keybuf     = buf8.new(($privkey ~~ m:g/../).map({ :16($_.Str)}));
    my $keyptr     = nativecast(CArray[uint8], $keybuf);
    my $buf        = buf8.new(0 xx 64);
    my $pubkey_ptr = nativecast(Pointer, $buf);
    my $ctx        = self.ctx(:type('sign'));

    my $res = secp256k1_ec_pubkey_create($ctx, $pubkey_ptr, $keyptr);

    die X::PublicKey.new(:payload('generation failed')) unless $res;

    @!key = self.convert_carray(:arr(nativecast(secp256k1_pubkey, $pubkey_ptr).data), :len(64));

    secp256k1_context_destroy($ctx);

    return buf8.new(@!key);
}

method verify_private_key(Str :$privkey!) {
    die X::PrivateKey.new(:payload('invalid hex')) unless $privkey && $privkey ~~ m/^^ <xdigit> ** 64 $$/;

    my $keybuf = buf8.new(($privkey ~~ m:g/../).map({ :16($_.Str)}));
    my $keyptr = nativecast(CArray[uint8], $keybuf);
    my $ctx    = self.ctx(:type('sign'));

    my $res = secp256k1_ec_seckey_verify($ctx, $keyptr);

    secp256k1_context_destroy($ctx);

    return $res;
}

method serialize_public_key_to_compressed(buf8 :$pubkey!, Bool :$cmp = True) returns CArray[uint8] {

    my $SECP256K1_FLAGS_TYPE_COMPRESSION = 1 +< 1;
    my $SECP256K1_FLAGS_BIT_COMPRESSION  = 1 +< 8;
    my $SECP256K1_EC_UNCOMPRESSED        = $SECP256K1_FLAGS_TYPE_COMPRESSION;
    my $SECP256K1_EC_COMPRESSED          = $SECP256K1_FLAGS_TYPE_COMPRESSION +| $SECP256K1_FLAGS_BIT_COMPRESSION;

    my UInt $buffer_len = $cmp ?? 33 !! 65;

    my $len    = CArray[uint32].new([$buffer_len]);
    my $output = buf8.new(0 xx $buffer_len);

    my $output_ptr = nativecast(CArray[uint8], $output);
    my $length_ptr = nativecast(Pointer[uint32], $len);

    my $ctx = self.ctx(:type('sign'));
    my $res = secp256k1_ec_pubkey_serialize($ctx, $output_ptr, $length_ptr, CArray[uint8].new($pubkey.list), $cmp ?? $SECP256K1_EC_COMPRESSED !! $SECP256K1_EC_UNCOMPRESSED);

    die X::PublicKey.new(:payload('serialization failed')) unless $res;

    secp256k1_context_destroy($ctx);

    return $output_ptr;
}

method compressed_public_key(buf8 :$pubkey!, Bool :$cmp = True) returns buf8 {
    my UInt $len = $cmp ?? 33 !! 65;

    my $cmpress_ptr       = self.serialize_public_key_to_compressed(:pubkey($pubkey), :$cmp);
    my @serialized_pubkey = self.convert_carray(:arr($cmpress_ptr), :$len);

    return buf8.new(@serialized_pubkey);
}

method ecdsa_sign(Str :$privkey!, Str :$msg!, Bool :$recover = False) returns buf8 {
    die X::PrivateKey.new(:payload('invalid hex')) unless $privkey && $privkey ~~ m/^^ <xdigit> ** 64 $$/;

    die X::Message.new(:payload('invalid hex')) unless $msg && $msg ~~ m/^^ <xdigit> ** 64 $$/;

    my $res;

    my $keybuf = buf8.new(($privkey ~~ m:g/../).map({ :16($_.Str)}));
    my $msgbuf = buf8.new(($msg ~~ m:g/../).map({ :16($_.Str)}));

    my $keyptr = nativecast(CArray[uint8], $keybuf);
    my $msgptr = nativecast(CArray[uint8], $msgbuf);

    my $bufsz  = $recover ?? 65 !! 64;
    my $buf    = buf8.new(0 xx $bufsz);
    my $sigptr = nativecast(Pointer, $buf);

    my $ctx = self.ctx(:type('sign'));

    if $recover {
        $res    = secp256k1_ecdsa_sign_recoverable($ctx, $sigptr, $msgptr, $keyptr, Nil, Nil);

        die X::Message::Signature.new(:payload('failure while recoverable signing message')) unless $res;

        @!sig_r = self.convert_carray(:arr(nativecast(secp256k1_ecdsa_recoverable_signature, $sigptr).data), :len($bufsz));
    }
    else {
        $res  = secp256k1_ecdsa_sign($ctx, $sigptr, $msgptr, $keyptr, Nil, Nil);

        die X::Message::Signature.new(:payload('failure while signing message')) unless $res;

        @!sig = self.convert_carray(:arr(nativecast(secp256k1_ecdsa_signature, $sigptr).data), :len($bufsz));
    }

    secp256k1_context_destroy($ctx);

    return buf8.new($recover ?? @!sig_r !! @!sig);
}

method verify_ecdsa_sign(buf8 :$pubkey!, Str :$msg!, buf8 :$sig!) returns Bool {
    die X::Message.new(:payload('invalid hex')) unless $msg && $msg ~~ m/^^ <xdigit> ** 64 $$/;

    my $msgbuf  = buf8.new(($msg ~~ m:g/../).map({ :16($_.Str)}));
    my $msgcarr = CArray[uint8].new($msgbuf.list);

    my $ctx = self.ctx(:type('verify'));
    my $res = secp256k1_ecdsa_verify($ctx, nativecast(Pointer, $sig), $msgcarr, nativecast(Pointer, $pubkey));

    secp256k1_context_destroy($ctx);

    return $res.Bool;
}

method signature_normalize(buf8 :$sig!) returns buf8 {
    my $buf    = buf8.new(0 xx 64);
    my $sigout = nativecast(Pointer, $buf);

    my $ctx = self.ctx(:type('sign'));
    my $res = secp256k1_ecdsa_signature_normalize($ctx, $sigout, nativecast(Pointer, $sig));

    @!sig_n = self.convert_carray(:arr(nativecast(secp256k1_ecdsa_signature, $sigout).data), :len(64));

    secp256k1_context_destroy($ctx);

    return $res ?? buf8.new(@!sig_n) !! $sig;
}

method signature_serialize(buf8 :$sig!) returns buf8 {
    my $output     = buf8.new(0 xx 64);
    my $output_ptr = nativecast(CArray[uint8], $output);

    my $ctx = self.ctx(:type('verify'));
    my $res = secp256k1_ecdsa_signature_serialize_compact($ctx, $output_ptr, nativecast(Pointer, $sig));

    die X::Message::Signature::Serialize.new(:payload('failure while signature serializing')) unless $res;

    secp256k1_context_destroy($ctx);

    return $output;
}

method recoverable_signature_serialize(buf8 :$sig!) returns Hash {
    my $output     = buf8.new(0 xx 64);
    my $output_ptr = nativecast(CArray[uint8], $output);

    my $rec = CArray[uint32].new([0]);

    my $ctx = self.ctx(:type('verify'));
    my $res = secp256k1_ecdsa_recoverable_signature_serialize_compact($ctx, $output_ptr, nativecast(Pointer[int32], $rec), nativecast(Pointer, $sig));

    die X::Message::Signature::Serialize.new(:payload('failure while signature serializing')) unless $res;

    secp256k1_context_destroy($ctx);

    return {signature => $output, recovery => $rec[0]}
}

method recoverable_signature_convert(buf8 :$sig!) returns buf8 {
    my $buf    = buf8.new(0 xx 64);
    my $sigout = nativecast(Pointer, $buf);

    my $ctx = self.ctx(:type('sign'));
    my $res = secp256k1_ecdsa_recoverable_signature_convert($ctx, $sigout, nativecast(Pointer, $sig));

    die X::Message::Signature::Convert.new(:payload('failure while signature converting')) unless $res;

    @!sig = self.convert_carray(:arr(nativecast(secp256k1_ecdsa_signature, $sigout).data), :len(64));

    secp256k1_context_destroy($ctx);

    return buf8.new(@!sig);
}

method ecdsa_recover(buf8 :$pubkey!, buf8 :$sig!, Str :$msg!) returns Bool {
    die X::Message.new(:payload('invalid hex')) unless $msg && $msg ~~ m/^^ <xdigit> ** 64 $$/;

    my $msgbuf  = buf8.new(($msg ~~ m:g/../).map({ :16($_.Str)}));
    my $msgcarr = CArray[uint8].new($msgbuf.list);

    my $ctx = self.ctx(:type('verify'));
    my $res = secp256k1_ecdsa_recover($ctx, nativecast(Pointer, $pubkey), nativecast(Pointer, $sig), $msgcarr);

    secp256k1_context_destroy($ctx);

    return $res.Bool;
}

method recoverable_signature_parse_compact(buf8 :$sig!, UInt :$recovery) returns buf8 {
    my $ecdsa_signature     = buf8.new(0 xx 65);
    my $ecdsa_signature_ptr = nativecast(Pointer, $ecdsa_signature);

    my $input64 = CArray[uint8].new($sig.list);

    my $ctx = self.ctx(:type('sign'));
    my $res = secp256k1_ecdsa_recoverable_signature_parse_compact($ctx, $ecdsa_signature_ptr, $input64, $recovery);

    secp256k1_context_destroy($ctx);

    return $ecdsa_signature;
}

# secp256k1 generic
sub secp256k1_context_create(uint32 $flags) returns Pointer is native(secp256k1-lib) {*}
sub secp256k1_context_destroy(Pointer $ctx) is native(secp256k1-lib) {*}
sub secp256k1_ec_pubkey_create(Pointer $ctx, Pointer $pubkey, CArray[uint8] $seckey) returns int32 is native(secp256k1-lib) {*}
sub secp256k1_ec_seckey_verify(Pointer $ctx, CArray[uint8] $seckey) returns int32 is native(secp256k1-lib) {*}
sub secp256k1_ec_pubkey_serialize(Pointer $ctx, CArray[uint8] $output, Pointer[uint32] $output_len, CArray[uint8] $pubkey, uint32 $flags) returns int32 is native(secp256k1-lib) {*}
sub secp256k1_ecdsa_sign(Pointer $ctx, Pointer $sig, CArray[uint8] $msghash, CArray[uint8] $seckey, Pointer, Pointer) returns int32 is native(secp256k1-lib) {*}
sub secp256k1_ecdsa_verify(Pointer $ctx, Pointer $sig, CArray[uint8] $msghash, Pointer $pubkey) returns int32 is native(secp256k1-lib) {*}
sub secp256k1_ecdsa_signature_normalize(Pointer $ctx, Pointer $sigout, Pointer $sigin) returns int32 is native(secp256k1-lib) {*}
sub secp256k1_ecdsa_signature_serialize_compact(Pointer $ctx, CArray[uint8] $output64, Pointer $sig) returns int32 is native(secp256k1-lib) {*}

# secp256k1 recoverable
sub secp256k1_ecdsa_recoverable_signature_parse_compact(Pointer $ctx, Pointer $sig, CArray[uint8] $input64, int32 $recid) returns int32 is native(secp256k1-lib) {*}
sub secp256k1_ecdsa_recoverable_signature_convert(Pointer $ctx, Pointer $sig, Pointer $sig_r) returns int32 is native(secp256k1-lib) {*}
sub secp256k1_ecdsa_recoverable_signature_serialize_compact(Pointer $ctx, CArray[uint8] $output64, Pointer $rec, Pointer $sig) returns int32 is native(secp256k1-lib) {*}
sub secp256k1_ecdsa_sign_recoverable(Pointer $ctx, Pointer $sig, CArray[uint8] $msghash, CArray[uint8] $seckey, Pointer, Pointer) returns int32 is native(secp256k1-lib) {*}
sub secp256k1_ecdsa_recover(Pointer $ctx, Pointer $pubkey, Pointer $sig, CArray[uint8] $msghash) returns int32 is native(secp256k1-lib) {*}
