# Raku binding to optimized C library for ECDSA

Raku binding to optimized C library for ECDSA signatures and secret/public key operations on curve secp256k1.

## Synopsys

```perl
my $data = {
    key => '6fcc37ea5e9e09fec6c83e5fbd7a745e3eee81d16ebd861c9e66f55518c19798',
    msg => '1c8aff950685c2ed4bc3174f3472287b56d9517b9c948127319a09a7a36deac8'
};

my $secp256k1 = Bitcoin::Core::Secp256k1.new;
my $signature = $secp256k1.ecdsa_sign(:privkey($data<key>), :msg($data<msg>));

$signature.gist.say;
```

## Further work

For now `Bitcoin::Core::Secp256k1` module does not cover full `secp256k1` API in C, so I'm working on this and will deliver complete binding ASAP.

## License

Module `Bitcoin::Core::Secp256k1` is free and open source software, so you can redistribute it and/or modify it under the terms of the [The Artistic License 2.0](https://opensource.org/licenses/Artistic-2.0).

## Credits

1. https://github.com/bitcoin-core/secp256k1
2. `secp256k1` C API: https://github.com/bitcoin-core/secp256k1/blob/master/include/secp256k1.h

## Author

Please contact me via [Matrix](https://matrix.to/#/@k.narkhov:matrix.org) or [LinkedIn](https://www.linkedin.com/in/knarkhov/). Your feedback is welcome at [narkhov.pro](https://narkhov.pro/contact-information.html).
